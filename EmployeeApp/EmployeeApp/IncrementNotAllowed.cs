﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeApp
{
   public class IncrementNotAllowed:Exception
    {
        public IncrementNotAllowed() { }

        public IncrementNotAllowed(string message)
            : base(message)
        {

        }
    }
}
