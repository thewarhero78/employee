﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace EmployeeApp
{

    public partial class Form1 : Form
    {
        string connectionString = "Data Source=DSK-1096\\SQL2019;Initial Catalog=Emp;Integrated Security=True";
        SqlConnection connection;
        SqlDataAdapter dataadapter;
        DataSet ds;

        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            updateDs();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double doubleValue = 0;
            if (textBox1.Text.Length == 0 || Double.TryParse(textBox1.Text, out doubleValue) == false)
            {
                MessageBox.Show("Invalid Input");
            }
            doubleValue = Convert.ToDouble(textBox1.Text);
            int age = 0;
            decimal salary = 0.0M;
            string queryString =
            "SELECT Age,Salary from employee where id=@id";

            // set to the console window.
            using (SqlConnection connection =
           new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        age = reader.GetInt32(0);
                        salary = reader.GetDecimal(1);

                    }
                    reader.Close();
                    if (age < 30)
                    {
                        throw new IncrementNotAllowed("Increment is not allowed!!");
                    }
                    else
                    {
                        String query = "Update employee set salary=@sal where id=@id";
                        SqlCommand command1 = new SqlCommand(query, connection);
                        command1.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
                        command1.Parameters.AddWithValue("@sal", (Convert.ToDouble(salary) + (Convert.ToDouble(salary) * (doubleValue / 100))));
                        command1.ExecuteNonQuery();

                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    dataGridView1.Refresh();
                    textBox1.Text = "";

                }
            }


        }

        private void updateDs()
        {
            string sql = "SELECT * FROM Employee";
            connection = new SqlConnection(connectionString);

            dataadapter = new SqlDataAdapter(sql, connection);
            ds = new DataSet();
            connection.Open();
            dataadapter.Fill(ds, "Employee_table");
            connection.Close();
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Employee_table";
            comboBox1.DataSource = ds.Tables[0];
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection =
          new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    var name = nameTxt.Text;
                    var desig = desigTxt.Text;
                    int age = Convert.ToInt32(ageTxt.Text);
                    decimal sal = Convert.ToDecimal(salTx.Text);
                    DateTime da = dateTimePicker1.Value.Date;
                    String query = "Insert employee values(@name,@da,@desi,@sal,@age)";
                    SqlCommand command1 = new SqlCommand(query, connection);
                    command1.Parameters.AddWithValue("@name", name);
                    command1.Parameters.AddWithValue("@age", age);
                    command1.Parameters.AddWithValue("@sal", sal);
                    command1.Parameters.AddWithValue("@desi", desig);
                    command1.Parameters.AddWithValue("@da", da);
                    command1.ExecuteNonQuery();
                    MessageBox.Show("Employee Addedd!!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    updateDs();
                    nameTxt.Text = "";
                    desigTxt.Text = "";
                    ageTxt.Text = "";
                    salTx.Text = "";

                }


            }
        }
    }
}
